<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class getCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getCurrency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'getting currencies from http://phisix-api3.appspot.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currenciesArr = json_decode(file_get_contents("http://phisix-api3.appspot.com/stocks.json"),true);
        $currencies = [];
        $index = 0;
        foreach($currenciesArr['stock'] as $currency){
            $currencies[$index]['name'] = $currency['name'];
            $currencies[$index]['volume'] = $currency['volume'];
            $currencies[$index]['amount'] = round($currency['price']['amount'],2);
            $index++;
            if($index>50)
                break;
        }
        DB::table('currencies')->delete();
        DB::table('currencies')->insert($currencies);
    }
}
