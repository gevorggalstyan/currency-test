<?php

namespace App\Http\Controllers;

use App\Console\Commands\getCurrency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CurrencyController extends Controller
{

    public function index()
    {
        $currencies = DB::table('currencies')->get();
        return view('currency',['currencies' => $currencies]);
    }

    public function updateCurrency(){
        $currencyCommand = new getCurrency();
        $currencyCommand->handle();
        return back();
    }

}
