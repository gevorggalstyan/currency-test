<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    </head>
    <body>
     <div class="container">
        <h2 class="mt-5">Currency Table</h2>
        <a href="{{route('updateCurrency')}}" class="btn btn-dark float-right mb-4">Update</a>
        <table class="table mt-5 table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Volume</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($currencies as $currency)
                <tr>
                    <td>{{$currency->name}}</td>
                    <td>{{$currency->volume}}</td>
                    <td>{{$currency->amount}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </body>
</html>
