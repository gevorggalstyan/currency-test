<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 9/5/2018
 * Time: 1:37 PM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function indexAction(){

        return $this->render('page/index.html.twig');
    }

}